#include <Motor.h>
#include <Sensor.h>
#include <Encoders.h>
class SpeedReg
{
public:
	float Kp,Ki,Kd;
	float speed;
	int *impulses;
	int lastImpulses;
	long timeLast;
	Motor * engine;
	float desiredSpeed;
	float lastSpeed,lastfSpeed; //last speed and last filtered speed
	float sumError;
	SpeedReg(Motor & engine,int &impulses);
	void setSpeed(float desiredspeed);
	void update();
	void update(float desiredSpeed);
	void setPIDval(float Kp,float Ki,float Kd);
	void reset(float pp,float ii,float dd);
	void reset();
};
	


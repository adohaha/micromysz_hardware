#include <Sensor.h>
#include <Motor.h>
#include <Encoders.h>
#include <Spregone.h>


//int global_counts_m1; //used by Encoders
//int global_counts_m2;



Motor wheelL(6,7); //speed pin, direction pin
Motor wheelR(5,4);

Encoders enkodery;

/* setting speed controller,
global_counts_m1 ,m2 are a counters for encoder ticks, set by Encoders */


// En



unsigned long lastcontroltime; 

int  adc_key_val[5] ={
  30, 150, 360, 535, 760 };
int NUM_KEYS = 5;
int adc_key_in;
int key=-1;



void setup(void) 
{ 
  Serial.begin(57600);      //Set Baud Rate
 
  wheelR.init();
  wheelL.init();
  
enkodery.init(8,9,2,12); //rightA, rightB, leftA, leftB

   
   wheelL.setPWM(100);
   wheelR.setPWM(100);
lastcontroltime=millis()+500;
} 

void loop()
{
  adc_key_in = analogRead(7);    // read the value from the sensor  
  digitalWrite(13, HIGH);
  /* get the key */
  key = get_key(adc_key_in); 
  if(key==1)
  {
      wheelL.setPWM(100);
   wheelR.setPWM(100);
 }
 else
 {
     wheelL.setPWM(0);
   wheelR.setPWM(0);
 }
  

  
   Serial.print("encoder left = " );                       
  Serial.print(enkodery.impulsyLewy());   
   Serial.print("encoderright = " );                       
  Serial.println(enkodery.impulsyPrawy());   
}


int get_key(unsigned int input)
{   

  
  int k;
  for (k = 0; k < NUM_KEYS; k++)
  {
    if (input < adc_key_val[k])
    {  
      return k;  
    }
  }
  if (k >= NUM_KEYS)
    k = -1;     // No valid key pressed
  return k;
}

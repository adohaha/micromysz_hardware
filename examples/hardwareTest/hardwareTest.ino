#include <Sensor.h>
#include <Motor.h>

//Inicjalizacja silnika
int ML = 4;    //lewy kierunek
int EL = 5;    //lewy predkosc

int EP = 6;    //prawy kierunek
int MP = 7;    //prawy predkosc

//Sensor init
Sensor czujnik_S(0);
Sensor czujnik_L(3);
Sensor czujnik_P(1);
Motor silnikL(EL,ML);
Motor silnikP(EP,MP);

void setup(void) 
{ 
  Serial.begin(19200);      //Setting baudrate
  Serial.println("Run keyboard control");
  silnikP.init();
  silnikL.init();

} 
void loop(void) 
{
  if(Serial.available()){
    char val = Serial.read();
    switch(val){
    case 'q':
    Serial.println("Left engine -- forward");
    silnikL.setPWM(100);
    break;
    case 'a':
    Serial.println("Left engine -- back");
    silnikL.setPWM(-100);
    break;
    case 'z':
    Serial.println("Left engine - stop");
    silnikL.setPWM(0);
    break;
    case 'w':
    Serial.println("Right engine - forward");
    silnikP.setPWM(100);
    break;
    case 's':
    Serial.println("Right engine - back");
    silnikP.setPWM(-100);
    break;
    case 'x':
    Serial.println("Right engine - stop");
    silnikP.setPWM(0);
    break;
    case 'm':
    Serial.print("Left sensor: ");
    Serial.println(czujnik_L.odlegloscNoFiltr());
    Serial.print("Middle sensor: ");
    Serial.println(czujnik_S.odlegloscNoFiltr());
    Serial.print("Right sensor: ");
    Serial.println(czujnik_P.odlegloscNoFiltr());
    Serial.println(' ');
    break;
    }    
  }  
}


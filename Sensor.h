#ifndef SENSOR_H
#define SENSOR_H

class Sensor{
  public: 
  /*
    Konstruktor - tworzy obiekt reprezentujacy czujnik odleglosci.
    @param - int analogPin - wejscie analogowe czujnika
  */
  Sensor(int analogPin);
  Sensor();
  /*
  Pomiar odleglosci
  @return - float odleglosc w centymetrach
  */
  float odleglosc( void );
float odlegloscNoFiltr( void );
  void init(int analogPin);
  private:
	int Sharp_new;
int Sharp_old;
int licznik;
  //Zmienne przechowujące numery odpowiednich nóżek.
  int pin;
float Filtr_Medianowy5(float *pom);
};

#endif

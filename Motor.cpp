#include "Motor.h"
#include "Arduino.h"

Motor::Motor(int speedControl,int directionControl){
  this->speedControlReg = speedControl; 

  this->dirControlReg = directionControl;
}

Motor::Motor(){
}

void Motor::init(int speedControl,int directionControl){
  this->speedControlReg = speedControl; 
  this->dirControlReg = directionControl;
  pinMode(this->speedControlReg, OUTPUT);
  pinMode(this->dirControlReg, OUTPUT);
}
void Motor::init(){
  pinMode(this->speedControlReg, OUTPUT);
  pinMode(this->dirControlReg, OUTPUT);
}

void Motor::setPWM(int v){

  if(v > 255) v = 255;
  else if (v < -255) v = -255;

  if (v >= 0) {

    digitalWrite(this->dirControlReg,LOW);

    analogWrite(this->speedControlReg,v);
  }
  else if (v < 0) {
    digitalWrite(this->dirControlReg,HIGH);

    analogWrite(this->speedControlReg,(-v));
  }
}

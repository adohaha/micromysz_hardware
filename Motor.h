#ifndef MOTORS_H
#define MOTORS_H
#include "Arduino.h"

class Motor{
  public: 
  /*
    Konstruktor - tworzy obiekt reprezentujacy silnik.
    @param - int speedControl - wyjscie PWM kontrolujące prędkość silnika
    @param - int directionControl - wyjscie sterujące kierunkiem obrotu silnika 
  */

  Motor(int speedControl,int directionControl);
  Motor();
  //Inicjalizacja wyjsc cyfrowych
  void init (void);
  /*
    Ustawianie predkosci silnika. 
    @param int v - wypełnienie sygnału PWM. Zakres 255 (maksymalnie do przodu) : -255 (maksymalnie do tyłu); 0-zatrzymaj silnik.
  */
  void setPWM(int v);
 
  void init(int speedControl,int directionControl);
  int speedControlReg;
  int dirControlReg;
  private:
  //Zmienne przechowujące numey odpowiednich nóżek.

};

#endif

/*
  PololuWheelEncoders.cpp - Library for using Pololu Wheel Encoders.
*/
	
/*
 * Copyright (c) 2009-2012 Pololu Corporation. For more information, see
 *
 *   http://www.pololu.com
 *   http://forum.pololu.com
 *   http://www.pololu.com/docs/0J18
 *
 * You may freely modify and share this code, as long as you keep this
 * notice intact (including the two links above).  Licensed under the
 * Creative Commons BY-SA 3.0 license:
 *
 *   http://creativecommons.org/licenses/by-sa/3.0/
 *
 * Disclaimer: To the extent permitted by law, Pololu provides this work
 * without any warranty.  It might be defective, in which case you agree
 * to be responsible for all resulting costs and damages.
 */

#include <avr/io.h>
#include <avr/interrupt.h>
#include <stdlib.h>
#include <Arduino.h>
#include "Encoders.h"
/*
 * Pin Change interrupts
 * PCI0 triggers on PCINT7..0
 * PCI1 triggers on PCINT14..8
 * PCI2 triggers on PCINT23..16
 * PCMSK2, PCMSK1, PCMSK0 registers control which pins contribute.
 *
 * The following table is useful:
 *
 * Arduino pin  AVR pin    PCINT #            PCI #
 * -----------  ---------  -----------------  -----
 * 0 - 7        PD0 - PD7  PCINT16 - PCINT23  PCI2
 * 8 - 13       PB0 - PB5  PCINT0 - PCINT5    PCI0
 * 14 - 19      PC0 - PC5  PCINT8 - PCINT13   PCI1
 *
 */


static char global_m1a;
static char global_m2a;
static char global_m1b;
static char global_m2b;

int global_counts_m1;
int global_counts_m2;

static char global_last_m1;
static char global_last_m2;


struct IOStruct
{
	// if these aren't volatile, the compiler sometimes incorrectly optimizes away operations involving these registers:
	volatile unsigned char* pinRegister;
	volatile unsigned char* portRegister;
	volatile unsigned char* ddrRegister;
	unsigned char bitmask;
};



inline static void getIORegisters(struct IOStruct* io, unsigned char pin)
	{
		io->pinRegister = 0;
		io->portRegister = 0;
		io->ddrRegister = 0;
		io->bitmask = 0;

		if (pin < 8)			// pin 0 = PD0, ..., 7 = PD7
		{
			io->pinRegister = (unsigned char*)&PIND;
			io->portRegister = (unsigned char*)&PORTD;
			io->ddrRegister = (unsigned char*)&DDRD;
			io->bitmask = 1 << pin;
		}

#if defined(_ORANGUTAN_SVP) || defined(_ORANGUTAN_X2)

		else if (pin < 16)		// pin 8 = PB0, ..., 15 = PB7
		{
			io->pinRegister = (unsigned char*)&PINB;
			io->portRegister = (unsigned char*)&PORTB;
			io->ddrRegister = (unsigned char*)&DDRB;
			io->bitmask = 1 << (pin - 8);
		}
		else if (pin < 24)		// pin 16 = PC0, ..., 23 = PC7
		{
			io->pinRegister = (unsigned char*)&PINC;
			io->portRegister = (unsigned char*)&PORTC;
			io->ddrRegister = (unsigned char*)&DDRC;
			io->bitmask = 1 << (pin - 16);
		}
		else if (pin < 32)		// pin 24 = PA7, ..., 31 = PA0
		{
			io->pinRegister = (unsigned char*)&PINA;
			io->portRegister = (unsigned char*)&PORTA;
			io->ddrRegister = (unsigned char*)&DDRA;
			io->bitmask = 1 << (31 - pin);
		}

#else

		else if (pin < 14)		// pin 8 = PB0, ..., 13 = PB5 (PB6 and PB7 reserved for external clock)
		{
			io->pinRegister = (unsigned char*)&PINB;
			io->portRegister = (unsigned char*)&PORTB;
			io->ddrRegister = (unsigned char*)&DDRB;
			io->bitmask = 1 << (pin - 8);
		}
		else if (pin < 21)		// pin 14 = PC0, ..., 19 = PC5 (PC6 is reset, PC7 doesn't exist)
		{
			io->pinRegister = (unsigned char*)&PINC;
			io->portRegister = (unsigned char*)&PORTC;
			io->ddrRegister = (unsigned char*)&DDRC;
			io->bitmask = 1 << (pin - 14);
		}
#endif
	}

inline static unsigned char getInputValue(struct IOStruct* ioPin)
	{
		return *(ioPin->pinRegister) & ioPin->bitmask;
	}
inline static unsigned char isInputHigh(unsigned char pin)
	{
		struct IOStruct registers;
		getIORegisters(&registers, pin);
		return getInputValue(&registers);
	}


ISR(PCINT0_vect)
{
	unsigned char m1a_val = digitalRead(global_m1a);
	unsigned char m1b_val = digitalRead(global_m1b);

	unsigned char m2a_val = digitalRead(global_m2a);	
	unsigned char m2b_val = digitalRead(global_m2b);

	unsigned char current_m1 = (m1b_val << 1) + m1a_val;
	unsigned char current_m2 = (m2b_val << 1) + m2a_val;

	if(current_m1 != global_last_m1)
	{
	if( (global_last_m1 & 1) ^ ((current_m1 & 2) >> 1) )
	global_counts_m1--;
	else
	global_counts_m1++;
	}
	
	if(current_m2 != global_last_m2)
	{
	if( (global_last_m2 & 1) ^ ((current_m2 & 2) >> 1) )
	global_counts_m2--;
	else
	global_counts_m2++;
	}

	global_last_m1 = current_m1;
	global_last_m2 = current_m2;

}

ISR(PCINT1_vect,ISR_ALIASOF(PCINT0_vect));
ISR(PCINT2_vect,ISR_ALIASOF(PCINT0_vect));
#ifdef PCINT3_vect
ISR(PCINT3_vect,ISR_ALIASOF(PCINT0_vect));
#endif

static void enable_interrupts_for_pin(unsigned char p)
{
	// TODO: Unify this with the code in OrangutanPulseIn::start
	// that does the same thing, and move it to OrangutanDigital.

	struct IOStruct io;
	getIORegisters(&io, p);

#if defined(_ORANGUTAN_SVP) || defined(_ORANGUTAN_X2)
	if (io.pinRegister == &PINA)
		PCMSK0 |= io.bitmask;
	if (io.pinRegister == &PINB)
		PCMSK1 |= io.bitmask;
	if (io.pinRegister == &PINC)
		PCMSK2 |= io.bitmask;
	if (io.pinRegister == &PIND)
		PCMSK3 |= io.bitmask;
#else
	if (io.pinRegister == &PINB)
		PCMSK0 |= io.bitmask;
	if (io.pinRegister == &PINC)
		PCMSK1 |= io.bitmask;
	if (io.pinRegister == &PIND)
		PCMSK2 |= io.bitmask;
#endif

	// Preserving the old behavior of the library prior to 2012-08-21,
	// we make the line be an input but do not specify whether its pull-up
	// should be enabled or not.
	*io.ddrRegister &= ~io.bitmask;

	// For simplicity set all the bits in PCICR and let the enabling of
	// pin-change interrupts be solely controlled by PCMSKx bits.
	PCICR = 0xFF;
}

void Encoders::init(unsigned char prawyA, unsigned char prawyB, unsigned char lewyA, unsigned char lewyB)
{
	global_m1a = prawyA;
	global_m1b = prawyB;
	global_m2a = lewyA;
	global_m2b = lewyB;

	// disable interrupts while initializing
	cli();

	enable_interrupts_for_pin(prawyA);
	enable_interrupts_for_pin(prawyB);
	enable_interrupts_for_pin(lewyA);
	enable_interrupts_for_pin(lewyB);

	// initialize the global state
	global_counts_m1 = 0;
	global_counts_m2 = 0;
	

	global_last_m1 = (digitalRead(global_m1b) << 1) + digitalRead(global_m1a);
	global_last_m1 = (digitalRead(global_m2b) << 1) + digitalRead(global_m2a);
	// Clear the interrupt flags in case they were set before for any reason.
	// On the AVR, interrupt flags are cleared by writing a logical 1
	// to them.
	PCIFR = 0xFF;

	// enable interrupts
	sei();
}

int Encoders::impulsyPrawy()
{
	cli();
	int tmp = global_counts_m1;
	sei();
	return tmp;
}

int Encoders::impulsyLewy()
{   
	cli();
	int tmp = global_counts_m2;
	sei();
	return tmp;
	
 
}

int Encoders::impulsyResetPrawy()
{
	cli();
	int tmp = global_counts_m1;
	global_counts_m1 = 0;
	sei();
	return tmp;
}

int Encoders::impulsyResetLewy()
{
	cli();
	int tmp = global_counts_m2;
	global_counts_m2 = 0;
	sei();
	return tmp;
}

#include <Arduino.h>
#include "Spregone.h"
#include <math.h>


SpeedReg::SpeedReg(Motor & engine2,int &impulses2) 
        {
        impulses=&impulses2;
        engine=&engine2;
        this->lastSpeed=0;
        desiredSpeed=0;
        speed=0;
        sumError=0;
        timeLast=millis();
        Kp=450;
	Ki=1;
	Kd=0;
	lastImpulses=*impulses;
        }
void SpeedReg::setPIDval(float Kp,float Ki,float Kd)
	{
	this->Kp=Kp;
	this->Ki=Ki;
	this->Kd=Kd;
	}
	void SpeedReg::reset()
	{
	this->lastSpeed=0;
        speed=0;
        desiredSpeed=0;
        this->sumError=0;
        this->timeLast=millis();
	}
void SpeedReg::reset(float Kp,float Ki,float Kd)
{
        this->Kp=Kp;
	this->Ki=Ki;
	this->Kd=Kd;
       this->reset();
}
void SpeedReg::setSpeed(float desiredSpeed) //setting speed, will start moving!
{
this->desiredSpeed=desiredSpeed;
//this->update();
}
void SpeedReg::update(float desiredSpeed)
{
this->setSpeed(desiredSpeed);
this->update();
}
void SpeedReg::update()
{

        unsigned long timeNow=millis();
        unsigned int deltaT;
        int imp,delta;
        float deltaSpeed;
        float error;
        
        deltaT=timeNow-timeLast;
        
        cli();
	imp=*impulses;
	sei();
        delta=imp-lastImpulses;
        speed=(float)delta/deltaT;
        //speed=(lastSpeed+speed)/2;
        float fSpeed=(lastSpeed+speed)/2; //filtered Speed
        
        this->speed=speed;
        if(isnan(speed))
        {
        this->lastSpeed=0;
        speed=0;
        lastImpulses=imp;
        }
        error=desiredSpeed-fSpeed;
        deltaSpeed=lastfSpeed-fSpeed;
        sumError+=error*deltaT; // integration
        float reg=(float)Kp*error+Ki*sumError+Kd*deltaSpeed;
        if(reg>255)
        {
        reg=255;
        sumError-=error*deltaT; //stop integration
        }
        else if(reg<-255)
        {
        reg=-255;
        sumError+=error*deltaT; //stop integration
        }

        engine->setPWM(reg); 
        //saving values for next iteration
        this->lastSpeed=speed;
        this->lastfSpeed=fSpeed;
        this->timeLast=timeNow;
        this->lastImpulses=imp;
        



}

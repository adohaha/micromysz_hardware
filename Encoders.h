#ifndef ENCODERS_H
#define ENCODERS_H

extern int global_counts_m1;
extern int global_counts_m2;

class Encoders
{
  public:
	/*
	 * Constructor: does nothing.
	 */
	Encoders() { }

	/*
	 * Initializes the wheel encoders.  The four arguments are the
	 * four pins that the two wheel encoders are connected to, according
	 * to the Arduino numbering: Arduino digital pins 0 - 7 correpsond
	 * to port D pins PD0 - PD7, respectively.  Arduino digital pins 8
	 * - 13 correspond to port B pins PB0 - PB5.  Arduino analog
	 * inputs 0 - 5 are referred to as digital pins 14 - 19 (these are
	 * the enumerations you should use for this library) and
	 * correspond to port C pins PC0 - PC5.
	 *
	 * 
	 * init() may be called multiple times.
	 */
	static void init(unsigned char prawyA, unsigned char prawyB, unsigned char lewyA, unsigned char lewyB);

	/*
	 * Encoder counts are returned as integers.  For the Pololu wheel
	 * encoders, the resolution is about 3mm/count, so this allows a
	 * maximum distance of 32767*3mm or about 100m.  For longer
	 * distances, you will need to occasionally reset the counts using
	 * the functions below.
	 */
	static int impulsyPrawy();
	static int impulsyLewy();
        int * leftpulses;
        int * rightpulses;
	/*
	 * These functions get the number of counts and reset the stored
	 * number to zero.
	 */
	static int impulsyResetPrawy();
	static int impulsyResetLewy();

	/*
	 * These functions check whether there has been an error on M1 or
	 * M2; that is, if both m1a/m1b or m2a/m2b changed simultaneously.
	 * They return 1 if there was an error, then reset the error
	 * flag.
	 */
	static unsigned char checkErrorM1();
	static unsigned char checkErrorM2();


private:

};
#endif
